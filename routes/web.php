<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PostsController@index');

Route::get('/home', 'HomeController@index');

Route::get('/posts/create', 'PostsController@create');

Route::get('/posts/{id}', 'PostsController@show');

Route::post('/posts/{post}/comments', 'CommentsController@addComment');


Auth::routes();


Route::resource('posts', 'PostsController')->middleware('auth');

Route::resource('admin','AdminController')->middleware('auth');

Route::get('{id}', 'PostsController@category');
