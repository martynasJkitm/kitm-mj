<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Comment;
use App\post;
class CommentsController extends Controller
{
    public function addComment(Request $request, Post $post)
    {
        $this->validate($request, ['body' => 'required']);

        $post->comments()->create([
            'body' => $request->body,
            'ip_address' => $request->ip(),
            'user_name' => $request->input('user_name'),
            'post_id' => $post->id
        ]);

        return redirect("/posts/{$post->id}");
    }


}
